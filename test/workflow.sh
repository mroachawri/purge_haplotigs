exit; 
# If you're testing your installation of Purge Haplotigs then use the makefile
make test # to validate
make clean # to clean up

# Validation and test dataset
# The test dataset is from here: https://doi.org/10.5281/zenodo.1042847


# The test commands:
purge_haplotigs hist -b aligned.bam -g contigs.fa

purge_haplotigs cov -i aligned.bam.200.gencov -l 3 -m 20 -h 70

purge_haplotigs purge -g contigs.fa -c coverage_stats.csv -b aligned.bam -t 16

purge_haplotigs purge -g contigs.fa -c coverage_stats.csv -b aligned.bam -t 4 -o curated.WM -r repeats.bed

purge_haplotigs place -p curated.fasta -h curated.haplotigs.fasta -t 4

purge_haplotigs place -p curated.WM.fasta -h curated.WM.haplotigs.fasta -t 4 -o ncbi_placements.WM.tsv

md5sum -c validate.md5
